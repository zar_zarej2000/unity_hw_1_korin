using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game1
{

[RequireComponent(typeof(Unit))]
public class UserInput : MonoBehaviour
{
  public Vector3 speed_mult;
  Unit unit;

  void Start()
  {
    unit = GetComponent<Unit>();
  }

  void Update()
  {
    unit.speed = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
    unit.speed.Scale(speed_mult);
  }
}

}
