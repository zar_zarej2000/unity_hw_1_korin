﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game0
{

public class Game : MonoBehaviour
{
  public GameObject target_spawn_placeholder;
  public Hero hero;
  public Target origin;
  public Target true_origin;
  public float start_hero_speed = 0.2f;
  public float step_hero_speed = 0.05f;
  public float distance_between_targets = 1.2f;

  List<Target> targets = new List<Target>();

  int target_count = 1;

  void Start()
  {
    SetupGame();
  }

  void SetupGame(bool win = true)
  {
    if(!win)
      target_count = 1; //Reset game
    SpawnTargets();
    SetupHeroMovement();
    RandomizeTrueTargetPosition();
  }

  void SetupHeroMovement()
  {
    hero.x_a = targets[0].transform.position.x;
    hero.x_b = targets[targets.Count - 1].transform.position.x;
    hero.speed = Mathf.Sqrt(start_hero_speed + step_hero_speed * (targets.Count - 1));
  }

  void SpawnTargets()
  {
    foreach(var target in targets)
      GameObject.Destroy(target.gameObject);

    targets.Clear();

    SpawnTarget(true_origin);

    for(int i = 1; i < target_count; ++i)
      SpawnTarget(origin);

    var start_position_x = target_spawn_placeholder.transform.position.x -
      (distance_between_targets * target_count) / 2.0f;

    for(int i = 0; i < targets.Count && target_count > 1; ++i)
    {
      targets[i].transform.position = new Vector3(
        start_position_x + distance_between_targets * (float)i,
        target_spawn_placeholder.transform.position.y,
        target_spawn_placeholder.transform.position.z);
    }

    target_count++;
  }

  void RandomizeTrueTargetPosition()
  {
    int index = Random.Range(0, targets.Count);
    var true_target_position = targets[0].transform.position;
    targets[0].transform.position = targets[index].transform.position;
    targets[index].transform.position = true_target_position;
  }

  void SpawnTarget(Target origin)
  {
    var new_target = Target.Instantiate(origin);
    new_target.transform.position = target_spawn_placeholder.transform.position;
    targets.Add(new_target);
  }

  void ProcessInput()
  {
    if(Input.GetMouseButtonDown(0))
    {
      foreach(var target in targets)
      {
        if(!target.is_true_target)
          continue;

        if(Mathf.Abs(hero.transform.position.x - target.transform.position.x) < 1.0f)
        {
          SetupGame();
          return;
        }
      }

      SetupGame(win: false);
    }
  }

  void LateUpdate()
  {
    ProcessInput();
  }
}

}
