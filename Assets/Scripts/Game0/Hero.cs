using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game0
{

public class Hero : MonoBehaviour
{
  public float x_a;
  public float x_b;

  public GameObject laser;
  public SetLaserState(bool state);
  
  public float speed = 1.0f;

  void Update()
  {
    transform.position = new Vector3(Mathf.Lerp(x_a, x_b, Mathf.PingPong(Time.time * speed, 1.0f)), 0.0f, 0.0f);
  }
}

}
